/************************************************
 * Reads SPI ADCs and displays values
 *
 * Connection guide:
 *
 * Board function | Pin
 *                |
 * CONVST         | A3
 * SCLK           | A2
 * ADC DIN        | A5 OR gnd
 * ADC DOUT       | A4
 * 
 * Sends comma seperated ADC values over USB uart interface
 * Set buad rate to 115200
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"
#include "driverlib/ssi.h"
#include "driverlib/pin_map.h"
#include "driverlib/uart.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ssi.h"

/**************************
 * Defines
 */

#define SPI_PORT_BASE    GPIO_PORTA_BASE

#define CONVST_PIN  GPIO_PIN_3
#define CLK_PIN     GPIO_PIN_2
#define RX_PIN    GPIO_PIN_4
#define TX_PIN     GPIO_PIN_5

#define SPI_CLK_CONFIGURE   GPIO_PA2_SSI0CLK
#define SPI_RX_CONFIGURE    GPIO_PA4_SSI0RX

#define VALUES_COUNT (2)

/**************************
 * Prototypes
 */

static void sysInit(void);
static void adcInit(void);
static void uartInit(void);
static void readAdcs(void);
static void uartPrint(char * str);

/**************************
 * Globals
 */

uint16_t adcValues[VALUES_COUNT];


void main(void)
{
    char str[15];
    uint8_t count;

    // Init clock and peripherals
    sysInit();
    adcInit();
    uartInit();

    //

    for(;;)
    {
        readAdcs();

        for(count = 0; count < VALUES_COUNT; count++)
        {
            sprintf(str, "%.2f, ", (float)adcValues[count] / 65535.0 * 5.0 );
            uartPrint(str);
        }
        sprintf(str, "\r\n");
        uartPrint(str);

        // Wait 1 second
        SysCtlDelay(SysCtlClockGet() / 3);
    }
}
    
static void sysInit(void)
{
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    IntMasterDisable();

    // enable clocking
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);


    // wait for clocking to be ready
    while ((!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOA))) {};

    IntMasterEnable();
}

static void adcInit(void)
{
    uint32_t tmp;

    // Enable clocking
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_SSI0)) {};

    // Setup pins
    GPIOPinTypeSSI(SPI_PORT_BASE, CLK_PIN);
    GPIOPinTypeGPIOOutput(SPI_PORT_BASE, CONVST_PIN | TX_PIN); // CONVST and DIN manually controlled
    GPIOPinTypeGPIOInput(SPI_PORT_BASE, RX_PIN); // use Rx to poll status with busy indicator mode

    GPIOPinConfigure(SPI_CLK_CONFIGURE);
    GPIOPinConfigure(SPI_RX_CONFIGURE);

    // Set low initially
    GPIOPinWrite(SPI_PORT_BASE, CONVST_PIN | TX_PIN, 0);

    SSIConfigSetExpClk(SSI0_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_1,
            SSI_MODE_MASTER, 2000000, 16);

    SSIEnable(SSI0_BASE);

    // Read any residual data from the SSI port
    while (SSIDataGetNonBlocking(SSI0_BASE, &tmp)) {};
}

static void uartInit(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART0)) {};

    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);

    UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 115200,
            (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
             UART_CONFIG_PAR_NONE));
}


static void readAdcs(void)
{
    uint8_t count;
    uint32_t value;

    // Bring CONVST high to start conversion
    GPIOPinWrite(SPI_PORT_BASE, CONVST_PIN, 0xff);

    // Wait until the DOUT of the last ADC goes high
    //while (GPIOPinRead(SPI_PORT_BASE, RX_PIN) == 0) {};
    SysCtlDelay(SysCtlClockGet() / 3.0 * 0.01);

    // Set RX pin to SSI and read data in
    GPIOPinTypeSSI(SPI_PORT_BASE, RX_PIN);
    
    for (count = 0; count < VALUES_COUNT; count++)
    {
        // Read two bytes
        SSIDataPut(SSI0_BASE, 0xffff);
        SSIDataGet(SSI0_BASE, &value);

        adcValues[count] = (uint16_t)value;
    }

    GPIOPinWrite(SPI_PORT_BASE, CONVST_PIN, 0x0);
    GPIOPinTypeGPIOInput(SPI_PORT_BASE, RX_PIN);
}

static void uartPrint(char * str)
{
    char *currChar;

    currChar = str;

    while(*currChar != '\0')
    {
        UARTCharPut(UART0_BASE, *currChar);
        currChar++;
    }
}
